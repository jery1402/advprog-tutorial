package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class DriedClamsTest {
    private Clams clams;

    @Before
    public void setUp() {
        clams = new DriedClams();
    }

    @Test
    public void testHotClamsToStringMethod() {
        assertEquals(clams.toString(), "Dried Clams from North America");
    }
}