package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class BlackDoughTest {
    private Dough dough;

    @Before
    public void setUp() {
        dough = new BlackDough();
    }

    @Test
    public void testMiddleThickCrustDoughToStringMethod() {
        assertEquals(dough.toString(), "Black Dough for special flavor");
    }
}