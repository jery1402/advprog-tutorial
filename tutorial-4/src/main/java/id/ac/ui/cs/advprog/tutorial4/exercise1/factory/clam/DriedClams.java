package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class DriedClams implements Clams {

    public String toString() {
        return "Dried Clams from North America";
    }
}
