package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CvController {

    @GetMapping("/cv")
    public String cv(@RequestParam(name = "visitor", required = false)
                             String visitor, Model model) {
        if (visitor != null) {
            String pageTitle = visitor + ", I hope you are interested to hire me";
            model.addAttribute("pageTitle", pageTitle);
        } else {
            model.addAttribute("pageTitle", "This is my CV");
        }

        return "cv";

    }

}
