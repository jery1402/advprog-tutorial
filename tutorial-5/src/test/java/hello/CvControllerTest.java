package hello;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CvController.class)
public class CvControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void resumeWithVisitor() throws Exception {
        String test = "Budi, I hope you are interested to hire me";
        mockMvc.perform(get("/cv").param("visitor", "Budi"))
                .andExpect(content().string(containsString(test)));

        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("This is my CV")));
    }

    @Test(expected = AssertionError.class)
    public void resumeWithVisitorNegative() throws Exception {
        String test = "Budi, I hope you are interested to hire me";
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString(test)));

        mockMvc.perform(get("/cv").param("visitor", "Budi"))
                .andExpect(content().string(containsString("This is my CV")));
    }

}

