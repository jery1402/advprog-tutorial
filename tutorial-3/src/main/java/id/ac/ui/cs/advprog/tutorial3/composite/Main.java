package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

import java.util.List;

public class Main {
    private static Company company;
    private static Ceo luffy;
    private static Cto zorro;
    private static BackendProgrammer franky;
    private static BackendProgrammer usopp;
    private static FrontendProgrammer nami;
    private static FrontendProgrammer robin;
    private static UiUxDesigner sanji;
    private static NetworkExpert brook;
    private static SecurityExpert chopper;


    public static void main(String[] args) {
        company = new Company();

        luffy = new Ceo("Luffy", 500000.00);
        company.addEmployee(luffy);
        System.out.println(luffy.getName());

        zorro = new Cto("Zorro", 320000.00);
        company.addEmployee(zorro);
        System.out.println(zorro.getName());

        franky = new BackendProgrammer("Franky", 94000.00);
        company.addEmployee(franky);
        System.out.println(franky.getName());

        usopp = new BackendProgrammer("Usopp", 200000.00);
        company.addEmployee(usopp);
        System.out.println(usopp.getName());

        nami = new FrontendProgrammer("Nami",66000.00);
        company.addEmployee(nami);
        System.out.println(nami.getName());

        robin = new FrontendProgrammer("Robin", 130000.00);
        company.addEmployee(robin);
        System.out.println(robin.getName());

        sanji = new UiUxDesigner("sanji", 177000.00);
        company.addEmployee(sanji);
        System.out.println(sanji.getName());

        brook = new NetworkExpert("Brook", 83000.00);
        company.addEmployee(brook);
        System.out.println(brook.getName());

        chopper = new SecurityExpert("Chopper", 70000.00);
        company.addEmployee(chopper);
        System.out.println(chopper.getName());

        Employees[] arrayEmployeeComparation = {luffy, zorro, franky, usopp, nami,
                                                robin, sanji, brook, chopper};
        List<Employees> allEmployees = company.getAllEmployees();


        System.out.println("arrayEmployeeComparation size = " + arrayEmployeeComparation.length);
        System.out.println("allEmployees size = " + allEmployees.size());
   
        System.out.println("Salary total: " + company.getNetSalaries());
    }
    
}
