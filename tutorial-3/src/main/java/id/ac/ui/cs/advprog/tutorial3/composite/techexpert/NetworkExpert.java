package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class NetworkExpert extends Employees {
    //TODO Implement
    public NetworkExpert(String name, double salary) {
        this.name = name;
        this.salary = salary;
        this.role = "Network Expert";
        checkSalary();
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return salary;
    }

    public void checkSalary() {
        if (salary < 50000.00) {
            throw new IllegalArgumentException("Raise the salary!");
        }
    }
}
