package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {
    public Cto(String name, double salary) {
        //TODO Implement
        this.name = name;
        this.salary = salary;
        this.role = "CTO";
        checkSalary();
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return salary;
    }

    public void checkSalary() {
        if (salary < 100000.00) {
            throw new IllegalArgumentException("Raise the salary!");
        }
    }
}
